"""Util module."""
from fw_gear_freesurferator.xnat_logging import stderr_log,stdout_log
import os
import sys
import xnat

def get_freesurfer_license(project:str,fs_license:str):
    """ Get Freesurfer license from project resource and save to Freesurfer home
    project : str
        project id
    fs_license : str
        Freesurfer license file name

    """
    freesurfer_home=os.environ["FREESURFER_HOME"]
    host=os.environ["XNAT_HOST"]
    user=os.environ["XNAT_USER"]
    password=os.environ["XNAT_PASS"]

    with xnat.connect(host, user=user, password=password) as session:
        proj_resources=session.get_json(f"/data/projects/{project}/files")
        filenames = [fs_license]
        found=False

        # look for the specified file(s) in project resource files
        for result in proj_resources['ResultSet']['Result']:
            if result['Name'] in filenames:
                file_uri=result['URI']
                if file_uri:
                    session.download(file_uri, f"{freesurfer_home}/.license")
                    
                    if os.path.exists(f"/{freesurfer_home}/.license"):
                        stdout_log.info(f"Found and copied {result['Name']} to {freesurfer_home}")
                        found=True
                    else:
                        stdout_log.error(f"Unable to copy {result['Name']} to {freesurfer_home}")
                        sys.exit(1)

    if not found:
        stdout_log.error(f"Unable to find {fs_license} under project resources!")
        sys.exit(1)

def get_primary_nifti(scan_id:str,input_dir="/input/SCANS")-> str:
    """Get full path to nifti file given scan id

    Args:
        scan_id (str): scan id
        input_dir (str, optional): Defaults to "/input/SCANS".

    Returns:
        str: string path to nifti file
    """
    
    nifti_dir=f"{input_dir}/{scan_id}/NIFTI"

    if os.path.exists(nifti_dir):
        nifti_files = [os.path.join(nifti_dir, file) for file in os.listdir(nifti_dir) if file.endswith('.nii') or file.endswith('.nii.gz')]

        if len(nifti_files) == 0:
            stdout_log.error(f"No NIFTI files found for scan: {scan_id}")
            sys.exit(1)
        else:
            return nifti_files[0]
    else:
        stdout_log.error(f"NIFTI resource not found for scan: {scan_id}")
        sys.exit(1)

    
def get_nifti_from_type(scan_type:str,input_dir="/input/SCANS"):
    """Get nifti file(s) from the scan type aka series description

    Args:
        scan_type (str): scan type aka series description
        input_dir (str, optional): path that contains all scans. Defaults to "/input/SCANS".
    """

    nifti_files_list=[]
    # Get the list of scan folders
    scans_list = [scan for scan in os.listdir(input_dir) if os.path.isdir(os.path.join(input_dir, scan))]
    
    scan_type_found = False  # Flag to track if scan_type is found
    
    # Iterate through each scan
    for scanid in scans_list:
        scan_path = os.path.join(input_dir, scanid)
        
        # Check if the subfolder contains a 'NIFTI' folder
        nifti_folder_path = os.path.join(scan_path, 'NIFTI')
        
        if os.path.exists(nifti_folder_path):

            # Get the list of files in the 'NIFTI' folder
            nifti_files= os.listdir(nifti_folder_path)

            if len(nifti_files) > 0:
                nifti_files = [f for f in nifti_files if f.endswith('.nii') or f.endswith('.nii.gz')]

                # Iterate through each NIFTI file and check for the scan_type substring
                for nifti_file in nifti_files:
                    if scan_type in nifti_file:
                        stdout_log.info(f"Found '{nifti_file}' in scan '{scanid}' from '{scan_type}' input.")
                        scan_type_found = True
                        nifti_files_list.append(os.path.join(nifti_folder_path,nifti_file))

    
    if not scan_type_found:
        stdout_log.info(f"No NIFTI files found with the specified scan type: {scan_type}")
        sys.exit(1)
    else:
        return nifti_files_list

