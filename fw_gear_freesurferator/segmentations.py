import os
import sys
import subprocess as sp
from fw_gear_freesurferator.xnat_logging import stderr_log,stdout_log

join = os.path.join

# Segmentation functions
def run_gtmseg(subject_id: str, out_dir: str):
    """
    Runs gtm segmentation used for PETsurfer

    Returns:
        [None]: [None]
    """

    gtmfile = join(out_dir, subject_id, "mri", "gtmseg.mgz")

    if os.path.isfile(gtmfile):
        stdout_log.info(f"   ... not running gtmseg, {gtmfile} exists.")
    else:
        command = str(f"gtmseg --s {subject_id} ")
        stdout_log.info(f"   ... running gtmseg with this command: \n{command}")
        gtmseg_sp=sp.run(command, shell=True)

        if gtmseg_sp.returncode !=0:
            stdout_log.error("Non-zero return code. Exiting now.")
            sys.exit(1)