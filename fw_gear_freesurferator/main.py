"""Main module."""

import os
import shutil
import subprocess as sp
import sys

import fw_gear_freesurferator.segmentations as seg
from fw_gear_freesurferator.xnat_logging import stderr_log,stdout_log

def run_freesurfer(gear_inputs: dict, gear_config: dict):
    """
    It runs recon-all

    Returns:
        [None]: [None]
    """

    stdout_log.info("Now running recon-all")

    # Additionnal T1w files
    ADD_INPUT = gear_inputs["additional_t1w"]
    if not ADD_INPUT:
        ADD_INPUT=""
        
    # T2w file
    if gear_inputs["t2w_anatomical"]:
        ADD_INPUT = f"{ADD_INPUT} -T2 {gear_inputs['t2w_anatomical']} -T2pial "

    # Run Freesurfer-Recon-all
    command = str(
        f"recon-all "
        f"-i {gear_inputs['anat']} {ADD_INPUT} "
        f"-subjid {gear_config['subject_id']} "
        f"{gear_config['reconall_options']}"
    )
    stdout_log.info(f"This is the recon-all command: \n{command}")
    reconall_sp=sp.run(command, shell=True)

    if reconall_sp.returncode !=0:
        stdout_log.error("Non-zero return code. Exiting now.")
        sys.exit(1)


def run(gear_inputs: dict, gear_config: dict):
    """
    This container will run Freesurfer or it will use an existing zipped fs output.
    Then it creates several 3D ROIs based on atlases or other tools.

    The container will have the option to include a folder or a zip
    file with FS run already. If there is no zip, there should be the anatomical file
    and it will run the whole FS. If both are present, it will use the zip.

    Returns:
        Nothing
    """
    # Path and variable definition
    join = os.path.join
    output_dir = str(gear_inputs["output_dir"])
    mri_dir = join(output_dir, gear_config["subject_id"], "mri")

   
    subject_id = gear_config["subject_id"]
   
    # Obtain FREESURFER_HOME directory
    fshome = join(os.environ["FREESURFER_HOME"])

    # Specify the SUBJECTS_DIR name
    os.environ["SUBJECTS_DIR"] = output_dir

    # Several programs downstream need to have fsaverage,
    # do it for all here and at the end unlink/delete it
    # Create symbolink link to fsaverage to the output folder
    fsaveragelink = join(output_dir, "fsaverage")
    # Check if the symbolic link exists, if so, unlink it
    if os.path.islink(fsaveragelink):
        stdout_log.info("Unlinking existing link to fsaverage")
        os.path.unlink(fsaveragelink)

    if not os.path.isdir(fsaveragelink):
        stdout_log.info("Copying fsaverage to output_dir, it will be removed at the end. ")
        shutil.copytree(join(fshome, "subjects", "fsaverage"), fsaveragelink)

    # run recon-all
    run_freesurfer(gear_inputs, gear_config)

    stats_table_dir=join(output_dir,f"{subject_id}_stats")
    if not os.path.exists(stats_table_dir):
        os.makedirs(stats_table_dir)
        

    # Convert stats files to csv and copy to the output
    # Write aseg stats to a table
    stdout_log.info("Exporting stats files to csv...")
    cmd = (
        f"asegstats2table -s {subject_id} "
        f"--delimiter comma "
        f"--tablefile={join(stats_table_dir,subject_id+'_aseg_stats_vol_mm3.csv')} "
    )
    stdout_log.info(f"Running asegstats2table command: {cmd}")
    asegstats_sp=sp.run(cmd, shell=True)

    if asegstats_sp.returncode !=0:
        stdout_log.error("Non-zero return code. Exiting now.")
        sys.exit(1)

    # Parse the aparc files and write to table
    hemis = ["lh", "rh"]
    parcs = ["aparc.a2009s", "aparc"]
    for h in hemis:
        for p in parcs:
            fname = f"{subject_id}_{h}_{p}_stats_area_mm2.csv"
            cmd = (
                f"aparcstats2table -s {subject_id} "
                f"--hemi={h} "
                f"--delimiter=comma "
                f"--parc={p} "
                f"--tablefile={join(stats_table_dir, fname)} "
            )
            stdout_log.info(f"Running asegstats2table command: {cmd}")
            aparcstats_sp=sp.run(cmd, shell=True)

            if aparcstats_sp.returncode !=0:
                stdout_log.error("Non-zero return code. Exiting now.")
                sys.exit(1)
    # run gtm segmentation if enabled
    if gear_config["run_gtmseg"] == True:
        seg.run_gtmseg(subject_id, output_dir)
 
    sp.run(f"chmod -R 777 {output_dir}", shell=True)

    # Remove the fsaverage folder created at the beginning
    stdout_log.info("Removing fsaverage folder")
    if os.path.isdir(fsaveragelink):
        shutil.rmtree(fsaveragelink)

    return 0
