"""Parser module to parse gear config.json."""
from typing import Tuple
import argparse
import sys
from fw_gear_freesurferator.util import get_primary_nifti, get_nifti_from_type
from fw_gear_freesurferator.xnat_logging import stdout_log

parser = argparse.ArgumentParser()

# set in command.json
parser.add_argument('--input_dir', required=True,type=str)
parser.add_argument('--output_dir', required=True,type=str)

# From session
parser.add_argument('--project_id', required=True,type=str)
parser.add_argument('--session_label', required=True,type=str)

# user settable inputs
parser.add_argument('--input_scan', required=True,type=str)
parser.add_argument('--freesurfer_license_file', required=True,default="license.txt",type=str)
parser.add_argument('--t1w_additional', required=False,type=str)
parser.add_argument('--t2w_anatomical', required=False,type=str)

# configs
parser.add_argument('--reconall_options', required=True,type=str,default="-all -qcache",nargs="+")
parser.add_argument('--run_gtmseg', required=False, default="True")

args=parser.parse_args() 

def parse_config() -> Tuple[dict, dict]:
    """Parse inputs and config from command.json

    Returns:
        [tuple]: inputs dictionary and config dictionary
    """

    # Gear inputs
    gear_inputs = {
        "project_id":args.project_id,
        "freesurfer_license_file": args.freesurfer_license_file,
        "output_dir": args.output_dir,
        "input_dir":args.input_dir
    }

    # get nifti file path for input scan
    input_scanid=args.input_scan.split("/scans/")[1]
    input_nifti=get_primary_nifti(input_scanid)

    if input_nifti:
        gear_inputs["anat"]=input_nifti
    else:
        stdout_log.error(f"No NIFTI found in scan: {input_scanid}")
        sys.exit(1)

    additional_t1_cmd=""
    t1_niftis_list=[]

    if args.t1w_additional:
        # split t1 additional by comma
        t1_additional_split=args.t1w_additional.split(',')
        if len(t1_additional_split) > 0:
            # For each scan type or series description in the split, find any NIFTIs that match across all scans
            for st in t1_additional_split:
                scan_type=st.strip().replace(" ","_").replace("/","_")
                nifti_files=get_nifti_from_type(scan_type)
                # If any matching nifti files found, append to a list
                if len(nifti_files) > 0:
                    for val in nifti_files:
                        t1_niftis_list.append(val)
            # make sure none of the additional T1s, match the primary scan nifti 
            for t1 in t1_niftis_list:
                if input_nifti == t1:
                    stdout_log.error(f"Error! The input scan {input_nifti} cannot be passed in again as an additional T1w input.")
                    sys.exit(1)
            # unpack list into a string passed into recon-all, "-i {nifti_file_name}"
            additional_t1_cmd = " ".join(["-i " + str(f) for f in t1_niftis_list])                 
            gear_inputs["additional_t1w"]=additional_t1_cmd
        else:
            gear_inputs["additional_t1w"]=""
    else:
        gear_inputs["additional_t1w"]=""

    # get t2w nifti and check if it overlaps with the input nifti
    if args.t2w_anatomical:
        t2_split=args.t2w_anatomical.split(',')
        # Can only match on 1 nifti
        if len(t2_split) > 1:
            stdout_log.error(f"t2w_anatomical input can only accept 1 scan type matcher, {t2_split} was provided.")
            sys.exit(1)

        scan_type=t2_split[0].strip().replace(" ","_").replace("/","_")

        # Only get 1 nifti file from T2 if found. Since -T2 option in recon-all only accepts 1 T2 image
        t2_niftis=get_nifti_from_type(scan_type)
        t2_nifti=t2_niftis[0]   
        if len(t2_niftis) > 1:
            stdout_log.warning(f"Found {len(t2_niftis)} files for T2 anatomical: {t2_niftis} ." 
                               f"recon-all only allows for 1 T2 image. Defaulting to the first image: {t2_nifti}")

        if t2_nifti != input_nifti:
            gear_inputs["t2w_anatomical"]= t2_nifti
        elif t2_nifti == input_nifti:
            stdout_log.error(f"t2w_anatomical input cannot be the same as the input scan nifti {input_nifti}")
            sys.exit(1)
        else:
            gear_inputs["t2w_anatomical"]= ""
    else:
         gear_inputs["t2w_anatomical"]= ""

    # Gear configs
    gear_config = {
        "subject_id": args.session_label,
        "reconall_options": ' '.join(args.reconall_options)
    }

    if args.run_gtmseg == "True":
        gear_config["run_gtmseg"]=True
    elif args.run_gtmseg == "False":
        gear_config["run_gtmseg"]=False


    return gear_inputs, gear_config
