FROM python:3.10-slim as build

ENV FLYWHEEL="/flywheel/v0"
RUN mkdir -p ${FLYWHEEL}
WORKDIR ${FLYWHEEL}

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        "software-properties-common" \
        "build-essential" \
        "apt-transport-https" \
        "ca-certificates" \
        "gnupg" \
        "software-properties-common" \
        "ninja-build" \
        "git" \
        "zlib1g-dev" \
        "cmake" \
        "xvfb" \
        "tcsh" \
        "gcc" \
        "g++" \
        "libeigen3-dev" \
        "zlib1g-dev" \
        "libgl1-mesa-dev" \
        "libfftw3-dev" \
        "libtiff5-dev" \
        "libxt6" \
        "libxcomposite1" \
        "libfontconfig1" \
        "libasound2" \
        "dpkg" \
        "mercurial" \
        "subversion" \
        "curl" \
        "libcurl4" \
        "zip" \
        "bc" \
        "unzip" \
        "perl-modules" \
        "xcb" \
        "qtbase5-dev" \
        "qtchooser" \
        "qt5-qmake" \
        "qtbase5-dev-tools" && \
    rm -rf /var/lib/apt/lists/* 
    
# Install Freesurfer 7.4.1, exclude cuda and trctrain
RUN curl -fsSL --retry 5 https://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/7.4.1/freesurfer-linux-ubuntu20_amd64-7.4.1.tar.gz | tar -xz -C /opt/ --strip-components 1 \
         --exclude='freesurfer/lib/cuda' \ 
         --exclude='freesurfer/trctrain' 

# Some of these environment variables are not used for recon-all, however will still set them
# Set the diplay env variable for xvfb
ENV DISPLAY :50.0
ENV QT_QPA_PLATFORM xcb
ENV PATH /opt/freesurfer/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/freesurfer/fsfast/bin:/opt/freesurfer/tktools:/opt/freesurfer/mni/bin:/sbin:/bin:/opt/ants/bin
ENV FS_LICENSE /opt/freesurfer/.license

ENV OS Linux
ENV FS_OVERRIDE 0
ENV FSL_OUTPUT_FORMAT nii.gz
ENV MNI_DIR /opt/freesurfer/mni
ENV LOCAL_DIR /opt/freesurfer/local
ENV FSFAST_HOME /opt/freesurfer/fsfast
ENV MINC_BIN_DIR /opt/freesurfer/mni/bin
ENV MINC_LIB_DIR /opt/freesurfer/mni/lib
ENV MNI_DATAPATH /opt/freesurfer/mni/data
ENV FMRI_ANALYSIS_DIR /opt/freesurfer/fsfast
ENV PERL5LIB /opt/freesurfer/mni/lib/perl5/5.8.5
ENV MNI_PERL5LIB /opt/freesurfer/mni/lib/perl5/5.8.5
ENV XAPPLRESDIR /opt/freesurfer/MCRv97/X11/app-defaults
ENV MCR_CACHE_DIR /flywheel/v0/output/.mcrCache9.7
ENV MCR_CACHE_ROOT /flywheel/v0/output

ENV FREESURFER_HOME /opt/freesurfer

# Remove these unused files from Freesurfer home
RUN rm -rf ${FREESURFER_HOME}/models/* \
    && rm -rf ${FREESURFER_HOME}/fsfast/* \
    && rm -rf ${FREESURFER_HOME}/diffusion/*

# Copy in project repo
COPY requirements.txt $FLYWHEEL/
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

COPY run.py ${FLYWHEEL}/
COPY fw_gear_freesurferator ${FLYWHEEL}/fw_gear_freesurferator

LABEL org.nrg.commands="[{\"name\": \"Freesurferator\", \"label\": \"Freesurferator\", \"description\": \"Runs Freesurferator v1.0\", \"version\": \"1.0\", \"schema-version\": \"1.0\", \"image\": \"registry.gitlab.com/flywheel-io/clinical-solutions/xnat-cs-containers/freesurferator:1.0\", \"type\": \"docker\", \"command-line\": \"python run.py --input_dir /input --output_dir /output #FSLICENSE# #SESSIONLABEL# #PROJ# #RECON_ALL_OPTIONS# #RUNGTMSEG# #T1w# #T1W_additional# #T2W#\", \"mounts\": [{\"name\": \"in\", \"writable\": false, \"path\": \"/input\"}, {\"name\": \"out\", \"writable\": true, \"path\": \"/output\"}], \"environment-variables\": {}, \"ports\": {}, \"inputs\": [{\"name\": \"freesurfer_license_file\", \"description\": \"Freesurfer license file name\", \"type\": \"string\", \"default-value\": \"license.txt\", \"required\": true, \"replacement-key\": \"#FSLICENSE#\", \"command-line-flag\": \"--freesurfer_license_file\", \"command-line-separator\": \" \", \"select-values\": []}, {\"name\": \"reconall_options\", \"description\": \"Arguments to pass to recon-all\", \"type\": \"string\", \"default-value\": \"'-all -qcache -threads 8'\", \"required\": true, \"replacement-key\": \"#RECON_ALL_OPTIONS#\", \"command-line-flag\": \"--reconall_options\", \"command-line-separator\": \" \", \"select-values\": []}, {\"name\": \"SESSION_LABEL\", \"description\": \"Session label\", \"type\": \"string\", \"required\": true, \"replacement-key\": \"#SESSIONLABEL#\", \"command-line-flag\": \"--session_label\", \"command-line-separator\": \" \", \"select-values\": []}, {\"name\": \"PROJECT\", \"description\": \"Project ID\", \"type\": \"string\", \"required\": true, \"replacement-key\": \"#PROJ#\", \"command-line-flag\": \"--project_id\", \"command-line-separator\": \" \", \"select-values\": []}, {\"name\": \"run_gtmseg\", \"description\": \"Create an anatomical segmentation for the geometric transfer matrix (GTM)\", \"type\": \"select-one\", \"default-value\": \"True\", \"required\": true, \"replacement-key\": \"#RUNGTMSEG#\", \"command-line-flag\": \"--run_gtmseg\", \"command-line-separator\": \" \", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"T1w\", \"description\": \"Input (primary) anatomical scan\", \"type\": \"string\", \"required\": true, \"replacement-key\": \"#T1w#\", \"command-line-flag\": \"--input_scan\", \"command-line-separator\": \" \", \"select-values\": []}, {\"name\": \"T2w\", \"description\": \"T2w anatomical scan\", \"type\": \"string\", \"default-value\": \"\", \"required\": false, \"replacement-key\": \"#T2W#\", \"command-line-flag\": \"--t2w_anatomical\", \"command-line-separator\": \" \", \"select-values\": []}, {\"name\": \"T1w_additional\", \"description\": \"Additional T1w anatomical scans\", \"type\": \"string\", \"default-value\": \"\", \"required\": false, \"replacement-key\": \"#T1W_additional#\", \"command-line-flag\": \"--t1w_additional\", \"command-line-separator\": \" \", \"select-values\": []}], \"outputs\": [{\"name\": \"freesurferator\", \"description\": \"freesurferator\", \"required\": true, \"mount\": \"out\"}], \"xnat\": [{\"name\": \"Freesurferator\", \"description\": \"Runs Freesurferator v1.0\", \"contexts\": [\"xnat:imageSessionData\"], \"external-inputs\": [{\"name\": \"session\", \"description\": \"Input session\", \"type\": \"Session\", \"required\": true, \"provides-files-for-command-mount\": \"in\", \"load-children\": true}], \"derived-inputs\": [{\"name\": \"session-label\", \"description\": \"Session label used as Freesurfer subject id.\", \"type\": \"string\", \"required\": true, \"provides-value-for-command-input\": \"SESSION_LABEL\", \"load-children\": true, \"derived-from-wrapper-input\": \"session\", \"derived-from-xnat-object-property\": \"label\", \"multiple\": false}, {\"name\": \"project-id\", \"description\": \"Project id\", \"type\": \"string\", \"required\": true, \"provides-value-for-command-input\": \"PROJECT\", \"load-children\": true, \"derived-from-wrapper-input\": \"session\", \"derived-from-xnat-object-property\": \"project-id\", \"multiple\": false}, {\"name\": \"T1w_anatomical_scan\", \"description\": \"Input (primary) anatomical scan\", \"type\": \"Scan\", \"default-value\": \"\", \"required\": true, \"provides-value-for-command-input\": \"T1w\", \"user-settable\": true, \"load-children\": true, \"derived-from-wrapper-input\": \"session\", \"multiple\": false}], \"output-handlers\": [{\"name\": \"freesurferator\", \"accepts-command-output\": \"freesurferator\", \"as-a-child-of\": \"session\", \"type\": \"Resource\", \"label\": \"freesurferator\", \"tags\": []}]}], \"reserve-memory\": 6000, \"limit-cpu\": 8, \"container-labels\": {}, \"generic-resources\": {}, \"ulimits\": {}, \"secrets\": []}]"