#!/usr/bin/env python
"""The run script"""
import sys

from fw_gear_freesurferator.main import run
from fw_gear_freesurferator.parser import parse_config
from fw_gear_freesurferator.util import get_freesurfer_license
from fw_gear_freesurferator.xnat_logging import stdout_log

def main() -> None: 
    """Parses config and run"""
    gear_inputs, gear_config = parse_config()

    # get Freesurfer license and copy into Freesurfer home
    get_freesurfer_license(gear_inputs.get("project_id"),gear_inputs.get("freesurfer_license_file"))

    # run it
    e_code = run(gear_inputs, gear_config)

    if e_code == 0:
        stdout_log.info("Freesurfer processing is complete.")

# Only execute if file is run as main, not when imported by another module
if __name__ == "__main__":
        main()
