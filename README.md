# Freesurferator

This session-level XNAT container runs Freesurfer's cortical reconstruction on 3D T1w anatomical images. This container is based off version `0.1.2_7.3.2` of the [Freesurferator gear](https://gitlab.com/flywheel-io/scientific-solutions/gears/freesurferator/-/tree/0.1.2_7.3.2?ref_type=tags), and uses Freesurfer version 7.4.1. In addition to reconstruction this container can optionally create an anatomical segmentation for the geometric transfer matrix ("gtmseg").


- [Freesurferator](#freesurferator)
  - [Summary](#summary)
  - [Inputs](#inputs)
      - [Additional T1w, T2w inputs](#additional-t1w-t2w-inputs)
  - [Configuration](#configuration)
  - [Outputs](#outputs)
  - [Citation](#citation)

## Summary
- A Freesurfer license is required to run this container. The license must be saved as a project resource. The container will specifically look for the file provided in the  [Freesurfer_license_file](#inputs) input. The license file can be saved under any project resource folder name.
  - <img src="./images/freesurfer_license_proj_resrc.png" width=25% />
- The *T1w_anatomical_scan* input must have a NIFTI scan resource (generated through the dcm2niix container). The first NIFTI file found in that resource will be used for this input.
- Additional T1w anatomical images and a T2/FLAIR image can be passed into recon-all though the *T1w_additional* and *T2w_anatomical* inputs. These scans must contain a NIFTI resource generated through dcm2niix container. See [inputs](#additional-t1w-t2w-inputs) for more information on how to include them.
- If `Freesurferator` is re-run on a session, any pre-existing [outputs](#outputs) will be overwritten.
- Performance:
  - `reserve-memory` in the command.json was set to 6000 (6GB)
  - `limit-cpu` in the command.json was set to 8
  - `--threads` was set to 8 by default in the *reconall_options* config. More threads can be provided through that config, however `reserve-memory` and `limit-cpu` need to reflect that change.


## Inputs

- *T1w_anatomical_scan*
  - __Required__: *True*
  - __Type__: *Dropdown*
  - __Description__: *Input (primary) anatomical scan*

- *Freesurfer_license_file*
  - __Required__: *True*
  - __Type__: *String*
  - __Default__: *license.txt*
  - __Description__: *FreeSurfer license file saved as a project resource*

- *T1w_additional*
  - __Required__: *False*
  - __Type__: *String*
  - __Description__: *Additional T1w anatomical scans*
  - __Notes__ : *Must be provided as a string, encapsulated by quotes*

 
- *T2w_anatomical*
  - __Required__: *False*
  - __Type__: *String*
  - __Description__: *T2w anatomical scan*
  - __Notes__ : *Must be provided as a string, encapsulated by quotes*


#### Additional T1w, T2w inputs
- To pass in additional T1w images set *T1w_additional* equal to the series description of the scan. For example: 
 - <img src="./images/matching.png" width=25% />

   <img src="./images/matching2.png" width=25% />

    - To include T1w images from scans 18 and 20, set *T1w_additional* to `"T1_tir_axial, T1_fl2d_Ax Gd"` by separating each matching series description with a comma, and then encapsulating the input in quotes. `Freesurferator` will then find **all matching NIFTI images** from scans. If it can't find any matching image, `Freesurferator` won't proceed and will give an error.
    - Only 1 T2w image can be sent to recon-all. Thus only provide 1 matcher in the *T2w_anatomical* input. For example to include scan 2 as the T2 image, set *T2w_anatomical* to `"T2 axial"`. If `Freesurferator` can't find any matching images, it won't proceed and will give an error.
  

## Configuration

- *reconall_options*
  - __Description__: *Command line options to recon-all*
  - __Default__: *'-all -qcache -threads 8'*
  - **Notes**: *This config must be provided as a string, encapsulated by quotes. See [recon-all wiki](https://surfer.nmr.mgh.harvard.edu/fswiki/recon-all) for possible options.*

- *run_gtmseg*
  - __Description__: *Create an anatomical segmentation for the geometric transfer matrix (GTM)*
  - __Default__: *True*

## Outputs
- All outputs are saved under the **freesurferator** session resource
  - <img src="./images/output_fs.png" width=40% />

    - The reconstruction is saved under the {session-label} folder
    - The {session-label}_stats folder contains these files converted to CSV format:
      - Areas of regions in left, right hemispheres for aparc and aparc.a2009s parcellations
      - Segmentation brain volumes (aseg stats)
  
## Citation

*For citation information, please visit: https://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferMethodsCitation*

